
ifdef CONFIG_TOOLS_PYGRILL

# Targets provided by this project
.PHONY: pygrill clean_pygrill

# Add this to the "contrib_testing" target
tools: pygrill
clean_tools: clean_pygrill


pygrill:
	@echo
	@echo "==== Deploy PyGrill Tool ===="
	@cp tools/pygrill/*.py $(BUILD_DIR)/usr/python
	@rm -f $(BUILD_DIR)/usr/bin/pygrill.py >/dev/null 2>&1
	@cd $(BUILD_DIR)/usr/bin/ && \
		ln -s $(BUILD_DIR)/usr/python/pygrill.py
	@ls $(BUILD_DIR)/usr/python/pygrill.py
	@echo

clean_pygrill:
	@echo "==== Clean-up PyGrill Tool ===="
	@rm -f $(BUILD_DIR)/usr/bin/pygrill.py >/dev/null 2>&1
	@echo

else # CONFIG_TOOLS_PYGRILL

pygrill:
	$(warning tools/bbque-pygrill module disabled by BOSP configuration)
	$(error BOSP compilation failed)

endif # CONFIG_TOOLS_PYGRILL

