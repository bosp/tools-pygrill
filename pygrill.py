#!/usr/bin/python 
#
#       @file  pygrill.py
#      @brief  Barbeque RTRM Application Scheduling Grapher.
#
# This is a simple script which produces a scheduling graph out of a Barbeque RTRM
# log file.
#
#     @author  Patrick Bellasi (derkling), derkling@google.com
#
#   @internal
#     Created  09/07/2011
#    Revision  $Id: doxygen.templates,v 1.3 2010/07/06 09:20:12 mehner Exp $
#    Compiler  python 2.6.6
#     Company  Politecnico di Milano
#   Copyright  Copyright (c) 2011, Patrick Bellasi
#
# This source code is released for free distribution under the terms of the
# GNU General Public License as published by the Free Software Foundation.
# ============================================================================

"""
Barbeque RTRM Application Scheduling Grapher.

This is a simple script which produces a scheduling graph out of a Barbeque RTRM
log file.

Usage: PyGrill <trace.dat>
Where:
- <trace.dat>: is the trace file to be parsed to produce the scheduling graph
"""

import sys
import getopt
import awk
import string

from random import random, randint
from time import sleep
from pyx import *

# Platform resources
CLUSTERS_COUNT = 3
CLUSTERS_PES = 4
# User preferences
EVENTS_COUNT = 40

# Our drawing canvas
SWIMLINE_HEIGHT = 14*unit.w_pt
EVENT_WIDTH = 14*unit.w_pt

# Define graph geometry based on platform data and user preferences
GRILL_HEIGHT = CLUSTERS_COUNT*CLUSTERS_PES*SWIMLINE_HEIGHT
GRILL_WIDTH = EVENTS_COUNT*EVENT_WIDTH

# Coords center
X_CENTER = 0.05*GRILL_WIDTH
Y_CENTER = 0.15*GRILL_HEIGHT

# Setup priority level gradients
prio_colors = [
    # RED range
    ("P0 (Highest)", color.cmyk.BrickRed),
    # ORANGE range
    ("P1", color.cmyk.Yellow),
    # GREEN range
    ("P2", color.cmyk.OliveGreen),
    # VIOLET range
    ("P3", color.cmyk.Lavender),
    # BLUE range
    ("P4", color.cmyk.CornflowerBlue),
    # BROWN range
    ("P5 (Lowest)", color.cmyk.Tan)
    ]


# Setup or drawing canvas
c = canvas.canvas()

class Usage(Exception):
    def __init__(self, msg):
        self.msg = msg

class AppsLegend:
    def __init__(self, x = X_CENTER, y = (Y_CENTER - 20*unit.w_pt),
                 w = 400*unit.w_pt, h = 50*unit.w_pt):
        self.x_pos = self.x_org = x
        self.y_pos = self.y_org = y
        self.width = w
        self.height = h
        # Set of applications already added to this legend
        self.apps = set()

        leg_border = path.rect(self.x_org, self.y_org,
                               self.width, -self.height)
        c.fill(leg_border, [color.cmyk.White])
        c.text(self.x_pos, self.y_pos,
               "Last schedule", [text.halign.boxleft, text.valign.top])

        # Legend limit
        self.y_min = (self.y_org - self.height)
        #marker = path.rect(self.x_pos, self.y_min,
        #                   self.x_pos + 20*unit.w_pt, -1*unit.w_pt)
        #c.fill(marker, [color.cmyk.Red])

        self.y_pos = self.y_org - 1.2*SWIMLINE_HEIGHT
        
    def AddApp(self, graph, spe):

        # Avoid duplicated entries
        if ((spe.pid, spe.name) in self.apps):
            return

        print "Add app %d:%s to legend" % (spe.pid, spe.name)    
        self.apps.add((spe.pid, spe.name))

        # Draw app marker
        exc_icon = path.rect(self.x_pos, self.y_pos,
                               EVENT_WIDTH/3, SWIMLINE_HEIGHT/3)
        c.fill(exc_icon, [graph.GetColor(spe.pid, spe.eid),
                            color.transparency(0.4)])

        # Drawing app lable
        app_lable = "%02d:%s[%d]" % (spe.pid, spe.name, spe.awm)
        app_lable = string.replace(app_lable, "_", "\_")
        print "Add legend for %s..." % (app_lable)    
        c.text(self.x_pos + EVENT_WIDTH/2, self.y_pos,
               app_lable, [text.halign.boxleft, text.size.scriptsize])

        # Legend limit
        #c.stroke(path.line(self.x_pos, (self.y_org - self.height),
        #                   self.x_pos + 20*unit.w_pt, (self.y_org - self.height)))

        # Update position for next app
        self.y_pos -= SWIMLINE_HEIGHT/2
        if (self.y_pos < self.y_min):
            self.y_pos = self.y_org - 1.2*SWIMLINE_HEIGHT
            self.x_pos += 100*unit.w_pt

class AppsTimeLine:
    def __init__(self, x = X_CENTER, y = (Y_CENTER - 30*unit.w_pt),
                 w = 400*unit.w_pt, h = 50*unit.w_pt):
        self.x_pos = self.x_org = x
        self.y_pos = self.y_org = y
        self.width = w
        self.height = h
        # The map of applications already added to the timeline:
        # {"pid:name": swimline}
        self.apps = {}
        # The map of last event count drawn for each application
        # {"pid:name": event_idx}
        self.last_event = {}
    
        #atl_border = path.rect(self.x_org, self.y_org,
        #                       self.width, -self.height)
        #c.fill(atl_border, [color.cmyk.Gray])

        # Draw X axis lable
        c.text(X_CENTER+GRILL_WIDTH, self.y_pos,
               "Aplications TimeLine",
               [text.halign.boxright])
        self.y_pos -= 5*unit.w_pt

        # Draw X axis
        self.y_org = self.y_pos
        self.x_org = X_CENTER
        x_axis = path.path(path.moveto(X_CENTER, self.y_org),
                           path.lineto(X_CENTER+GRILL_WIDTH+10*unit.w_pt,
                               self.y_pos))
        self.y_pos -= 0.5*unit.w_pt
        c.stroke(x_axis, [deco.earrow(angle=30)])

        # Legend limit
        self.y_min = (self.y_org - self.height)
        #marker = path.rect(self.x_pos, self.y_min,
        #                   self.x_pos + 20*unit.w_pt, -1*unit.w_pt)
        #c.fill(marker, [color.cmyk.Red])
    
    def AddApp(self, graph, spe):
        # Lookup if this app has already a swimline
        app_name = "%02d:%s" % (spe.pid, spe.name)
        if (app_name in self.apps):
            y_pos = self.apps[app_name]
        else:
            y_pos = self.__SetNewSwimline(spe, app_name)

        # Check if the application as already been added for this event
        if ((app_name in self.last_event) and
                (self.last_event[app_name] == spe.sev_idx)):
            return

        # Add marker for this application
        self.__AddMarker(spe, graph, y_pos)
        

    def __SetNewSwimline(self, spe, app_name):
        print "Add ATL swimline [%d] for [%s]" % (len(self.apps), app_name)
        y_pos = self.y_pos
        self.apps[app_name] = y_pos
        self.__DrawSwimline(app_name, y_pos)
        self.y_pos -= SWIMLINE_HEIGHT

        x_pos = X_CENTER + (EVENT_WIDTH*spe.sev_idx)
        y_line = path.path(path.moveto(x_pos, self.y_pos),
                path.lineto(X_CENTER+GRILL_WIDTH, self.y_pos))
        c.stroke(y_line, [style.linestyle.dashed])
        return y_pos

    def __DrawSwimline(self, app_name, y_pos):
        # Drawing PE lable
        app_lable = string.replace(app_name, "_", "\_")
        c.text(X_CENTER-5*unit.w_pt, y_pos-SWIMLINE_HEIGHT/1.5, app_lable,
                [text.halign.boxright, text.size.small])

    def __AddMarker(self, spe, graph, y_pos):
        # Draw app marker
        x_pos = X_CENTER + (EVENT_WIDTH*spe.sev_idx)
        exc_icon = path.rect(x_pos, y_pos,
                EVENT_WIDTH, -SWIMLINE_HEIGHT)
        c.fill(exc_icon, [graph.GetColor(spe.pid, spe.eid),
            color.transparency(0.4)])
        # Write AWM
        c.text(x_pos+EVENT_WIDTH-2*unit.w_pt, y_pos-6*unit.w_pt,
               spe.awm, [text.halign.boxright, text.size.tiny])

        # Duration axes
        y_over = 0
        if ( y_pos+SWIMLINE_HEIGHT > self.y_org):
            y_over = 2*unit.w_pt
        duration_axis = path.path(
                    path.moveto(x_pos, y_pos+y_over),
                    path.lineto(x_pos, y_pos-SWIMLINE_HEIGHT))
        c.stroke(duration_axis,
                [color.cmyk.Gray, color.transparency(0.4)])

class ScheduleGraph():
    def __init__(self, clusters = CLUSTERS_COUNT, pes = CLUSTERS_PES):
        # Setup EXCs colors
        self.excs_colors = color.gradient.Hue
        # The amp of assigned colors (PID:Color)
        self.assigned_colors = {}
        # The vector of free colors
        self.available_colors = []
        # Number of platform clsuters
        self.clusters = clusters
        # Number of PEs per cluster
        self.pes = pes
        # Computing PEs swmilines height
        self.swimline_h = (GRILL_HEIGHT / (clusters*pes))

        # Init colors and draw grid
        self.__InitColors()
        self.__DrawGraphGrid()

    def __InitColors(self, count = 2*CLUSTERS_COUNT*CLUSTERS_PES):
        # Populate the available_colors vector
        # Trying to sample the colorspace by placing distant colors
        # and considering that we could have a maximun of (Clusters*PEs)
        # applications running concurrently
        steps = 1.3/6
        value = steps
    
        print "Adding ", count, " colors with step ", steps
        for idx in range(count):
            color = value%1.0
            print "Adding color #", idx, ": ", color
            self.available_colors.append(self.excs_colors.getcolor(color))
            value += steps

#    def DrawColorKeys(steps = 4):
#        x_pos = X_CENTER-20*unit.w_pt
#        y_pos = Y_CENTER-20*unit.w_pt
#        h_sample = SWIMLINE_HEIGHT/4
#    
#        # Draw colors key legend
#        c.text(x_pos, y_pos+5*unit.w_pt,
#               "Priority colors:",
#               [text.halign.boxleft])
#        
#        for (p, g) in prio_colors:
#            idx = 0.0
#    
#            # Draw priority legend
#            c.text(x_pos+20*unit.w_pt,
#                   y_pos-(steps/2+1)*h_sample,
#                   p, [text.halign.boxleft])
#            
#            for i in range(steps):
#                #print "Colors for %s: %f" % (p, idx)
#                idx += 1.0/steps
#                # Adjust Y position for new color sample
#                y_pos -= h_sample
#                # Draw the color sample
#                color_sample = path.rect(x_pos, y_pos,
#                               15*unit.w_pt, h_sample)
#                c.fill(color_sample,
#                       [g, color.transparency(0.1)])

    def __DrawGraphGrid(self):
    
        # Setup the figure border
        #border = path.rect(0, 0, 1.1*GRILL_WIDTH, 1.3*GRILL_HEIGHT)
        #UpdateGraph([border])
        
        # Draw X axis
        x_org = X_CENTER
        y_org = Y_CENTER + self.clusters * self.pes * self.swimline_h
        x_axis = path.path(path.moveto(x_org, y_org),
                           path.lineto(x_org + GRILL_WIDTH + 10*unit.w_pt, y_org))
        c.stroke(x_axis, [deco.earrow(angle=30)])
        c.text(X_CENTER+GRILL_WIDTH,
                #Y_CENTER-25*unit.w_pt,
                Y_CENTER + 5*unit.w_pt + self.clusters*self.pes*self.swimline_h,
                "Schedule [ms]", [text.halign.boxright])
        
        # Draw Y axis
        y_axis = path.path(path.moveto(X_CENTER, Y_CENTER),
                           path.lineto(X_CENTER, Y_CENTER+GRILL_HEIGHT))
        c.stroke(y_axis)
        c.text(X_CENTER+GRILL_WIDTH/2, Y_CENTER+GRILL_HEIGHT+30*unit.w_pt,
               "Barbeque RTRM Scheduling Trace",
               [text.halign.boxcenter, text.size.Large])
        
        # Draw PEs swimming lines
        for sl in range(0, self.clusters*self.pes):
            y_pos = Y_CENTER + sl*self.swimline_h
            y_line = path.path(path.moveto(X_CENTER, y_pos),
                               path.lineto(X_CENTER+GRILL_WIDTH, y_pos))
            if ((sl % self.pes) == 0):
                # Draw a CLUSTER separation swimline
                c.stroke(y_line)
            else:
                # Draw a PE separation swimline
                c.stroke(y_line, [style.linestyle.dashed])
                
            # Drawing PE lable
            pe_lable = "C%d:%02d" % (sl/self.pes, (sl%self.pes))
            c.text(X_CENTER-5*unit.w_pt, y_pos+self.swimline_h/3, pe_lable,
                   [text.halign.boxright, text.size.small])
       
    def GetColor(self, pid, eid):
        # Lookup for color being already assigned
        if (pid, eid) in self.assigned_colors.keys():
            print "Found color assigned to: %d:%d" % (pid, eid)
            return self.assigned_colors[(pid, eid)]
    
        print "Assign new color to: %d:%d" % (pid, eid)
        self.assigned_colors[(pid, eid)] = self.available_colors.pop(0)
        return self.assigned_colors[(pid, eid)]

    def MarkApp(self, event, spe):
        x_pos = X_CENTER + (EVENT_WIDTH*event)
        y_pos = Y_CENTER + (SWIMLINE_HEIGHT*(CLUSTERS_PES*spe.cluster) +
                            SWIMLINE_HEIGHT*spe.pe)
    
        # Draw application marker
        exc_marker = path.rect(x_pos, y_pos,
                               EVENT_WIDTH, SWIMLINE_HEIGHT)
        c.fill(exc_marker, [self.GetColor(spe.pid, spe.eid),
                            color.transparency(0.4)])
        
        # Write priority
        c.text(x_pos+2*unit.w_pt, y_pos+2*unit.w_pt,
               spe.prio, [text.halign.boxleft, text.size.tiny])
        #prio_marker = path.rect(x_pos+EVENT_WIDTH-6*unit.w_pt,
        #                       y_pos+SWIMLINE_HEIGHT-6*unit.w_pt,
        #                       4*unit.w_pt, 4*unit.w_pt)
        #c.fill(prio_marker, [prio_colors[prio][1]])

    def MarkDuration(self, se, time):
        x_pos = X_CENTER + (EVENT_WIDTH*(se.idx+1))
        y_pos = Y_CENTER - 5*unit.w_pt
        # Duration text
        c.text(x_pos, y_pos, time,
                [text.halign.boxright,
                    text.size.scriptsize,
                    trafo.rotate(70)])
        # Duration axes
        duration_axis = path.path(
                path.moveto(x_pos, Y_CENTER - 2*unit.w_pt),
                path.lineto(x_pos, Y_CENTER+GRILL_HEIGHT))
        c.stroke(duration_axis,
                [color.cmyk.Gray, color.transparency(0.4)])

    def CleanupColors(self, prev_se, curr_se):
        # Cleanup all un-used colors
        for (pid, eid) in prev_se.scheduledApps:
            if (pid, eid) in curr_se.scheduledApps:
                continue
            print "Releasing color for terminated: %d:%d" % (pid, eid)
            self.available_colors.append(self.assigned_colors[(pid, eid)])
            del self.assigned_colors[(pid, eid)]



class ScheduledPE:
    def __init__(self, sev_idx, pid, eid, prio, cluster, pe, awm = 0, name = "UNK", recipe = "UNK"):
        self.sev_idx = sev_idx
        self.pid = pid
        self.eid = eid
        self.prio = prio
        self.cluster = cluster
        self.pe = pe
        self.awm = awm
        self.name = name
        self.recipe = recipe

class ScheduleEvent:
    def __init__(self, idx):
        self.idx = idx
        self.duration = 0
        # The list of assigned PEs
        self.scheduledPEs = []
        # The set of APPs sceduled
        self.scheduledApps = set()
  
    def Add(self, spe):
        self.scheduledPEs.append(spe)
        self.scheduledApps.add((spe.pid, spe.eid))
        print "Scheduling PID: %03d, on (C:%02d, P:%02d)" %\
                (spe.pid, spe.cluster, spe.pe)

    # Update the graph with this Schedule Event information
    def UpdateGraph(self, graph):
        for spe in self.scheduledPEs:
            graph.MarkApp(self.idx, spe)

    def SetDuration(self, time):
        self.duration = time

def GetApp(pa, ca):
    # Get a Random PID in the set e*[0..10]
    pid = 1000+randint(0,100)
    # Check if this application was already running in the
    # previous instant
    if pid in pa:
        ca[pid] = pa[pid]
    elif pid not in ca:
        # Get a Randome Prio in the set [0..5]
        ca[pid] = randint(0,5)

    return pid

def TestGrill2():
    # The graph to generate
    sg = ScheduleGraph()
    # The list if schedule events
    events = []

    prevEventApps = {}
    pev = None
    for ev in range(20):

        print "===== Schedule event %d" % (ev)
    
        # Build a new (empty) event
        cev = ScheduleEvent(len(events))
        currEventApps = {}

        # Random initialize esch PE
        for cl in range(CLUSTERS_COUNT):
            for pe in range(CLUSTERS_PES):

                # Schedule an app on this PE with certain probability
                if (random() > 0.6):
                    continue

                # Get an applicatio to be scheduled
                pid = GetApp(prevEventApps, currEventApps)

                # Scheduled a new PE and add to this event
                pe = ScheduledPE(ev, pid, pid, currEventApps[pid], cl, pe)
                cev.Add(pe)
                    

        # Updating events set tracker
        prevEventApps = currEventApps

        # Release unused colors
        if (pev != None):
            sg.CleanupColors(pev, cev)

        # Draw current event applications
        cev.UpdateGraph(sg)

        # Save current event for future run
        events.append(cev)
        pev = cev

        print "===== Wait schedule termination..."
        # TODO put here the code to parse a new event

        # Mark the schedule duration
        sg.MarkDuration(pev, randint(1,300))


    # Write canvas on PDF
    c.writePDFfile("graph")
        # Sleep a while before next run
        #sleep(1)

def TestGrill():
    InitColors()
    DrawGraphBase(CLUSTERS_COUNT,CLUSTERS_PES)

    prevEventApps = {}
    for ev in range(40):
        currEventApps = {}
        appsToMark = []

        # Mark duration of previous event
        if (ev):
            MarkDuration(ev-1, randint(1,300))
            
        for cl in range(CLUSTERS_COUNT):
            for pe in range(CLUSTERS_PES):
                # Schedule an app on this PE with certain probability
                if (0.1 < random()):
                    # Get a Random PID in the set e*[0..10]
                    pid = pe*randint(0,10)
                    # Check if this application was already running in the
                    # previous instant
                    if pid in prevEventApps:
                        currEventApps[pid] = prevEventApps[pid]
                    elif pid not in currEventApps:
                        # Get a Randome Prio in the set [0..5]
                        currEventApps[pid] = randint(0,5)
                    appsToMark.append(ScheduledPE(ev, pid, pid, currEventApps[pid], cl, pe))
                    #MarkApp(ev, pid, prio, cl, pe)
                    print "Scheduling %03d, (%02d, %02d)" % (pid, cl, pe)
                    
        # Cleanup all un-used colors
        for pid in prevEventApps:
            if pid in currEventApps:
                continue
            print "Releasing color for terminated app ", pid
            available_colors.append(assigned_colors[pid])
            del assigned_colors[pid]

        # Updating events set tracker
        prevEventApps = currEventApps

        # Draw current event applications
        for app in appsToMark:
            #MarkApp(ev, app.pid, app.prio, app.cluster, app.pe)
            MarkApp(ev, app)

    # Write canvas on PDF
    c.writePDFfile("graph")
        # Sleep a while before next run
        #sleep(1)

class BbqParser:
    def __init__(self):
        # Build the AWk parser
        self.parser = awk.AwkFileInput()
        # Parse PE assignment
        self.parser.add("^.+arch\.tile"
                "(?P<tile>\d)\.cluster(?P<cluster>\d)\.pe(?P<pe>\d)"
                "\s+\:\s+(?P<use>\d)"
                "\s+\|\s+(?P<tot>\d) "
                "\| (?P<pid>\d+):(?P<exc>[^:]+):(?P<eid>\d+),(?P<prio>\d+),(?P<awm>\d+)",
                self.__Assignment)
        # Trigger schedule plotting
        self.parser.add(".+Sync Time:",
                self.__Plot)
        # Parse new shcedule event
        self.parser.add(".+Schedule Run-time:\s+(?P<time>\d+)",
                self.__ScheduleTime)

        # The graph to generate
        self.sg = ScheduleGraph()
        # The applications timeline to generate
        self.atl = AppsTimeLine()
        # The list if schedule events
        self.events = []
        # Initial (empty) event
        self.cev = ScheduleEvent(0)
        # The previous event (for color management)
        self.pev = None

    def Parse(self):
        print "Parsing BBQ logfile..."
        self.parser.processinput()

    def __Assignment(self, tile, cluster, pe,
            use=0, tot=0, pid=0, prio=0, exc="", eid=0, awm=0):
        print "%d.%d.%d => %d/%d by %d:%d (exc: %s, awm: %d)" %\
            (int(tile), int(cluster), int(pe), int(use), int(tot),
                    int(pid), int(eid), exc, int(awm))

        # Scheduled a new PE and add to this event
        spe = ScheduledPE(len(self.events), int(pid), int(eid), int(prio), int(cluster), int(pe), int(awm), exc)
        self.cev.Add(spe)

        # Add application to the timeline
        self.atl.AddApp(self.sg, spe)
    
    def __Plot(self):
        
        # Release unused colors
        if (self.pev != None):
            self.sg.CleanupColors(self.pev, self.cev)

        # Draw current event applications
        self.cev.UpdateGraph(self.sg)

        # Save current event for future run
        self.events.append(self.cev)
        self.pev = self.cev

        # Write canvas on PDF
        c.writePDFfile("graph")

    def __ScheduleTime(self, time):
        print "Last schedule run-time: %d[ms]" %(int(time))
        
        # Mark the schedule duration
        self.sg.MarkDuration(self.pev, int(time))

        # Build a new (empty) event
        self.cev = ScheduleEvent(len(self.events))

def main(argv=None):
    if argv is None:
        argv = sys.argv

    # parse command line options
    try:
        try:
            opts, args = getopt.getopt(argv[1:], "h", ["help"])
        except getopt.error, msg:
             raise Usage(msg)

        # process options
        for o, a in opts:
            if o in ("-h", "--help"):
                print __doc__
                return 0
            
        # process arguments
        #for arg in args:
        #    process(arg) # process() is defined elsewhere

    except Usage, err:
        print >>sys.stderr, err.msg
        print >>sys.stderr, "for help use --help"
        return 2

    #TestGrill()
    #TestGrill2()

    bp = BbqParser()
    bp.Parse()



if __name__ == "__main__":
    sys.exit(main())
